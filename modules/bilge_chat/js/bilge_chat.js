(function ($) {

	$(document).ready(function(){
		var answer_count = 0;
		$('#bilge_chat_input').keyup(function(e){
		    if(e.keyCode == 13)
		    {
//		    	$('#bilge_chat_input').attr("disabled");
		    	answer_count++;
		        var text = $(this).val();
		        $(this).val("");
		        $("#bilge_chat_content ul").append("<li><span class='you'>You: </span><p>"+text+"</p></li>");
		        var bilge_li = $("#bilge_chat_content ul").append("<li id='answer_"+answer_count+"'><span class='bilge'>Bilge: </span><p class='loading'></p></li>");
		        $("#bilge_chat_content_wrapper").animate({ scrollTop: $("#bilge_chat_content").height() }, "fast");
		        $.post( "message", 
		          { message: text } 
		        )
		        .done(function( data ) {
//		          var tags = data;
//		          var items = "";
//		          var item_length = 10;
//		          for (i = 0; i < item_length; i++) 
//		          { 
//		        	  for(var k in data[i]){
//		        		  items += k+" - "+ data[i][k]+ " / ";
//		        	  }
//		          }
//		          items = (items=="") ? "No results" : items;
		        	
		          $("#answer_"+answer_count).html("<span class='bilge'>Bilge: </span><p>"+data+"</p>");
		          $("#bilge_chat_content_wrapper").animate({ scrollTop: $("#bilge_chat_content").height() }, "fast");
//		          $('#bilge_chat_input').removeAttr("disabled");
		        });
		    }
		});
	});
	
})(jQuery);
