package nlp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import opennlp.tools.cmdline.PerformanceMonitor;
import opennlp.tools.cmdline.parser.ParserTool;
import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.parser.ParserFactory;
import opennlp.tools.parser.ParserModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSSample;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.tokenize.WhitespaceTokenizer;
import opennlp.tools.util.InvalidFormatException;
import opennlp.tools.util.ObjectStream;
import opennlp.tools.util.PlainTextByLineStream;
import opennlp.tools.util.Span;
import php.java.servlet.RemoteHttpServletContextFactory;


/**
 * Servlet implementation class Hello
 */
@WebServlet("/Hello")
public class Hello extends HttpServlet {
	public static List<opennlp.tools.parser.Parse> nounPhrases;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Hello() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		String type = request.getParameter("test");
		String test = "I have a nice car";
		Hello.Parse(test);
		response.getWriter().write(test);
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RemoteHttpServletContextFactory ctx = new RemoteHttpServletContextFactory(this,
		getServletContext(), request, request, response);

		response.setHeader("X_JAVABRIDGE_CONTEXT", ctx.getId());
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");

		try { 
		  ctx.handleRequests(request.getInputStream(), response.getOutputStream()); 
		} finally { 
		  ctx.destroy(); 
		}
	}
	
	public static String NlpPOS(String args) {
	    POSModel model = new POSModelLoader().load(new File("/Library/Tomcat/webapps/NlpJobs/WEB-INF/models/en-pos-maxent.bin"));
	    PerformanceMonitor perfMon = new PerformanceMonitor(System.err, "sent");
	    POSTaggerME tagger = new POSTaggerME(model);
	
	    String input = args;
	    ObjectStream<String> lineStream =
	            new PlainTextByLineStream(new StringReader(input));
	
	    perfMon.start();
	    String line;
	    try {
			while ((line = lineStream.read()) != null) {

			    String whitespaceTokenizerLine[] = WhitespaceTokenizer.INSTANCE.tokenize(line);
			    String[] tags = tagger.tag(whitespaceTokenizerLine);

			    POSSample sample = new POSSample(whitespaceTokenizerLine, tags);
//	        System.out.println(sample.toString());
			    return sample.toString();

//	        perfMon.incrementCounter();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Failed";
		}
	    perfMon.stopAndPrintFinalResult();
	    return "Failed";
	}

	public static String[] SentenceDetect(String args) throws opennlp.tools.util.InvalidFormatException, IOException {
//		String paragraph = "Hi. How are you? This is Mike.";
		
		// always start with a model, a model is learned from training data
		InputStream is = new FileInputStream("/Library/Tomcat/webapps/NlpJobs/WEB-INF/models/en-sent.bin");
		SentenceModel model = new SentenceModel(is);
		SentenceDetectorME sdetector = new SentenceDetectorME(model);
		
		String sentences[] = sdetector.sentDetect(args);
		
		return sentences;
	}
	
	public static String[] Tokenize(String args) throws opennlp.tools.util.InvalidFormatException, IOException{
		InputStream is = new FileInputStream("/Library/Tomcat/webapps/NlpJobs/WEB-INF/models/en-token.bin");
	 
		TokenizerModel model = new TokenizerModel(is);
	 
		Tokenizer tokenizer = new TokenizerME(model);
	 
		String tokens[] = tokenizer.tokenize(args);
	 
		return tokens;
	}
	
	public static String[] findName(String args) throws IOException {
		InputStream is = new FileInputStream("/Library/Tomcat/webapps/NlpJobs/WEB-INF/models/en-ner-person.bin");
	 
		TokenNameFinderModel model = new TokenNameFinderModel(is);
		is.close();
	 
		NameFinderME nameFinder = new NameFinderME(model);
	 
		// somehow get the contents from the txt file 
//      and populate a string called documentStr
		String[] allNames = new String[0];
		String sentences[] = Hello.SentenceDetect(args);
		int i = 0;
		for (String sentence : sentences) {
		    String tokens[] = Hello.Tokenize(sentence);
		    Span nameSpans[] = nameFinder.find(tokens);
		    String test = Arrays.toString(Span.spansToStrings(nameSpans, tokens));
		    allNames[i] = test;
		    i++;
		    // do something with the names
//		    System.out.println("Found entity: " + Arrays.toString(Span.spansToStrings(nameSpans, tokens)));
		}
		return allNames;				
	}
	
	public static void Parse(String args) throws InvalidFormatException, IOException {
		// http://sourceforge.net/apps/mediawiki/opennlp/index.php?title=Parser#Training_Tool
		InputStream is = new FileInputStream("/Library/Tomcat/webapps/NlpJobs/WEB-INF/models/en-parser-chunking.bin");
		ParserModel model = new ParserModel(is);
		opennlp.tools.parser.Parser parser = ParserFactory.create(model);
		opennlp.tools.parser.Parse[] topParses = ParserTool.parseLine(args, parser, 1);
		
//		int i = 0;
		for (opennlp.tools.parser.Parse p : topParses)
		{
			p.show();
//			p.toString();
			Hello.nounPhrases.add(p);
//			result[i] = p.toString();
//			i++;
		}
		is.close();
		
//		return result;
	 
		/*
		 * (TOP (S (NP (NN Programcreek) ) (VP (VBZ is) (NP (DT a) (ADJP (RB
		 * very) (JJ huge) (CC and) (JJ useful) ) ) ) (. website.) ) )
		 */
	}
	
//	public static void getNounPhrases(opennlp.tools.parser.Parse p) {
////        if (p.getType().equals("NP")) {
//             nounPhrases.add(p);
////        }
//        for (opennlp.tools.parser.Parse child : p.getChildren()) {
//             getNounPhrases(child);
//        }
//    }


}
